
## 本科-公司金融-课程大纲-连玉君

&emsp;

## 1. 通知

- 作业-NEW
  - [Hola-Kola 个人案例分析](https://gitee.com/arlionn/CF/wikis/%E4%BD%9C%E4%B8%9A/Hola-Kola.md)，提交截止时间 `2024/11/8`
  - [HW-01](https://gitee.com/arlionn/CF/wikis/%E4%BD%9C%E4%B8%9A/HW01.md)，提交截止时间 `2024/11/12`

## 2. 课程概览


- **[课程主页](https://gitee.com/arlionn/CF)：** <https://gitee.com/arlionn/CF> 
- **任课教师：** 连玉君，arlionn@163.com 
- **助教：** 浦进博，<pjb201935019@163.com>
- **成绩构成：** 
  - 课堂参与和考勤 10%；课后习题 15%；小组案例报告 15%；期末考试 60%
- **教材：** 
  - **英文：** Berk, J., & DeMarzo, P. (2024). Corporate Finance (6th ed.). Pearson. [-Link-](https://www.pearson.com/en-us/subject-catalog/p/corporate-finance/P200000009791/9780137844906?tab=table-of-contents)
  - **中文：** 乔纳森•伯克、彼得•德马佐著，《公司理财》（上下册，第三版） 姜英兵译，中国人民大学出版社，ISBN：9787300196312, ([上](https://quqi.gblhgk.com/s/880197/1YUQk8Hr9iseBcJ5)，[下](https://quqi.gblhgk.com/s/880197/yAFra4o9dPfSjuzO))。
- **课件/案例材料下载：** 
  - <ftp://ftp.lingnan.sysu.edu.cn/>
  - 账号 = 密码 = lianyjst

### 重要信息：
- **个人作业和小组报告：** 请点击右上角 [wiki](https://gitee.com/arlionn/CF/wikis/Home) 按钮查看   
- **作业提交**：&#x1F449; A. [个人作业](https://workspace.jianguoyun.com/inbox/collect/aefeaf6e2d204bceac1c2196b1459a04/submit) &emsp; B. [小组案例报告](https://workspace.jianguoyun.com/inbox/collect/5823f62f1ed3469e86015cae5fe6f5f0/submit) 
  - 只需提交电子版即可；若需更新，只需将同名文件再次提交即可
  - 提交**截止时间**：当日 23:59，逾期不候


&emsp;

## 3. 个人作业

**a. 课后习题：** 请前往 Wiki [「**作业**」](https://gitee.com/arlionn/CF/wikis/Home) 页面查看。

**b. 个人案例分析**
- &#x1F449; P0-Hola-Kola (NPV)
  - A. 案例材料：请登录 FTP 下载
  - B. [ 案例思考题](https://gitee.com/arlionn/CF/wikis/%E4%BD%9C%E4%B8%9A/Hola-Kola.md)

&emsp;

## 4. 小组案例分析报告

### 4.1 规则
  - 全班同学分成 8 个小组
  - 每个小组选择一个案例 (同一个案例可以由两个小组选择)，完成案例分析报告。。
  - 老师确定时间展示，每小组 20-30 分钟

### 4.2 案例资料和要求
> **A.** 案例材料下载：请登录 FTP 查看。  
> **B.** 案例思考题和案例报告要求：[-点击查看-](https://gitee.com/arlionn/CF/wikis/%E5%B0%8F%E7%BB%84%E6%A1%88%E4%BE%8B/%E5%B0%8F%E7%BB%84%E6%A1%88%E4%BE%8B-%E6%8A%A5%E5%91%8A%E8%A6%81%E6%B1%82.md)  
> **C.** 案例报告提交：参见【作业提交：&#x1F34E;】   
> **D.** 提交截止时间：相应案例课堂展示前一天。


### 4.3 分组情况：  

==待更新==


&emsp;


## 5. 期末考试

- 考试时间：Week 18-19
- 考试形式：闭卷
- **要求和说明：**
  - 可以带一张 A4 小抄，正反面均可记录你认为有价值的信息；
  - 可以带计算器（包括简易计算器和金融计算器）；
  - 请将手机调至静音或关闭状态，考试期间不能使用手机；


&emsp;

## 6. 参考资料

除了指定的课本外，建议大家抽空阅读如下资料作为扩展。此外，[推荐书目](https://gitee.com/arlionn/CF/wikis/%E8%A1%A5%E5%85%85%E8%B5%84%E6%96%99/%E6%8E%A8%E8%8D%90%E4%B9%A6%E7%9B%AE) 也可以读读，以便了解中国资本市场。

- 达摩达兰-2015-4th-中文-应用公司理财，[-PDF-](https://www.jianguoyun.com/p/DaFA6N4QtKiFCBjIqqoD), [-英文版-](https://quqi.gblhgk.com/s/880197/YNTMFtc3BQRJWaTX)
- 保罗·阿斯奎思, 劳. A.韦斯. 公司金融:金融工具、财务政策和估值方法的案例实践[M]. 机械工业出版社, 2018. 书中的案例都很经典，分析的极为透彻。这些案例已经在 MIT 的 MBA 课堂上讨论了好多年。
- **估值** 达摩达兰, 投资估价：评估任何资产价值的工具和技术，2014，第 3 版，清华大学出版社. [中文-上](https://www.jianguoyun.com/p/Dcs4oboQtKiFCBjwkKsD)，[英文](https://www.jianguoyun.com/p/DZvsKpoQtKiFCBj2kKsD)。估值领域的经典之作。
- Holden-2015-**Excel Modeling** in Corporate Finance 5e， [-PDF-](https://quqi.gblhgk.com/s/880197/6JBMSQP2ipMAq8KR)，[-配套 Excel-xlsx-](https://quqi.gblhgk.com/s/880197/VsPpOXVU5aum2A5q)，[-配套 Excel-xls-](https://quqi.gblhgk.com/s/880197/XDngKwIjIa9q3i5x)
- 马永斌. 公司治理之道:控制权争夺与股权激励 (第二版)[M]. 清华大学出版社, 2018. [-Link-](https://item.jd.com/12466396.html)，公司治理，通俗易懂，提供了很多例子

<div STYLE="page-break-after: always;"></div>

&emsp;

## 7. 附：课程内容和进度

&emsp;

> **Note：** 周次以 [2024年秋-校历](https://www.sysu.edu.cn/images/2024qiu.jpg) 为准

| 周次    | 主题                                           | 学时 | 章节          |
| ------- | :--------------------------------------------- | :--- | :------------ |
| W1      | 公司财务简介                                   | 2    | Chp 1         |
| W2      | 财务决策与一价定律                             | 2    | Chp 3         |
| W3      | 货币时间价值                                   | 2    | Chp 4         |
| W4      | **国庆**                                       | —    | —             |
| W5      | 利率理论                                       | 2    | Chp 5         |
| W6      | 债券估值                                       | 2    | Chp 6         |
| W7      | 投资决策法则                                   | 2    | Chp 7         |
| W8      | 资本预算的基本原理                             | 2    | Chp 8         |
| W9      | Hola-Cola 案例讲解                             | 2    | 老师讲解+讨论 |
| W10-Mon | 股票估值                                       | 2    | Chp 9         |
| W10-Wed | **案例** 1：茅台估值                           | 2    | 小组展示+讨论 |
| W11-Mon | 资本结构 1：MM定理 - 1                         | 2    | Chp 14        |
| W11-Wed | 资本结构 1：MM定理 - 2                         | 2    | Chp 14        |
| W12-Mon | 资本结构 2：税收 - 1                           | 2    | Chp 15        |
| W12-Wed | 资本结构 2：税收 - 2                           | 2    | Chp 15        |
| W13-Mon | 资本结构 3：财务困境                           | 2    | Chp 16        |
| W13-Wed | 估值进阶                                       | 2    | Chp 18        |
| W14-Mon | 长期融资 I                                       | 2    | Chp 23-24     |
| W14-Wed | **案例** 2：HD困境 <br> **案例** 3：顺丰 IPO | 2    | 小组展示+讨论 |
| W15-Mon | 长期融资 II                                      | 2    | Chp 23-24     |
| W15-Wed | 公司治理                                       | 2    | 自编讲义     |
| W16-Mon | 股利政策                                       | 2    | Chp 17        |
| W16-Wed | **案例** 4：苹果股利                           | 2    | 小组展示+讨论 |
| W17-Mon | 论文讲解 + 复习                                | 2    | —             |
| W17-Wed | **元旦**                                       | —    | —             |
| W18-19  | 期末考试                                       | 0    | 闭卷考试      |


## 8. 案例思考题

> Note: 案例材料可以在 FTP 中下载。

> [查看案例思考题](https://gitee.com/arlionn/CF/wikis/%E5%B0%8F%E7%BB%84%E6%A1%88%E4%BE%8B/%E6%A1%88%E4%BE%8B%E6%80%9D%E8%80%83%E9%A2%98)